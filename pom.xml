<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2015 Eike Stepper (Berlin, Germany) and others.
  This program and the accompanying materials are made
  available under the terms of the Eclipse Public License 2.0
  which is available at https://www.eclipse.org/legal/epl-2.0/

  SPDX-License-Identifier: EPL-2.0

  Contributors:
    Eike Stepper - initial API and implementation
-->
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.eclipse.userstorage</groupId>
  <artifactId>parent</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <tycho.scmUrl>scm:git:https://git.eclipse.org/r/p/usssdk/org.eclipse.usssdk.git</tycho.scmUrl>
    <tycho-version>2.2.0</tycho-version>
    <jarsigner-version>1.3.1</jarsigner-version>

    <!-- OS-specific JVM flags, empty for the default case but redefined below -->
    <os-jvm-flags/>
  </properties>

  <pluginRepositories>
    <pluginRepository>
      <id>eclipse-maven-releases</id>
      <url>https://repo.eclipse.org/content/repositories/releases</url>
    </pluginRepository>
    <pluginRepository>
      <id>eclipse-cbi-releases</id>
      <url>https://repo.eclipse.org/content/repositories/cbi-releases</url>
    </pluginRepository>
    <!-- Snapshot Repos
    <pluginRepository>
      <id>eclipse-maven-snapshots</id>
      <url>https://repo.eclipse.org/content/repositories/snapshots</url>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>eclipse-cbi-snapshots</id>
      <url>https://repo.eclipse.org/content/repositories/cbi-snapshots</url>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </pluginRepository>
    -->
  </pluginRepositories>

  <repositories>
    <repository>
      <id>eclipse.license</id>
      <layout>p2</layout>
      <url>https://download.eclipse.org/cbi/updates/license</url>
    </repository>
  </repositories>

  <build>

    <plugins>
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-maven-plugin</artifactId>
        <extensions>true</extensions>
      </plugin>
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-source-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-source</id>
            <goals>
              <goal>plugin-source</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-surefire-plugin</artifactId>
        <configuration>
          <useUIHarness>false</useUIHarness>
          <trimStackTrace>false</trimStackTrace>
          <appArgLine>
            -consoleLog
            -eclipse.keyring /dev/null
          </appArgLine>
          <argLine>
            -Xmx1g
            -Dorg.eclipse.userstorage.credentialsProvider=org.eclipse.userstorage.tests.util.FixedCredentialsProvider
            -Dorg.eclipse.userstorage.internal.Session.debug=true
            -Dorg.eclipse.userstorage.quietSecureStorageException=true
            ${os-jvm-flags}
          </argLine>
        </configuration>
      </plugin>
    </plugins>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-maven-plugin</artifactId>
          <version>${tycho-version}</version>
          <extensions>true</extensions>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-packaging-plugin</artifactId>
          <version>${tycho-version}</version>
          <configuration>
            <format>'v'yyyyMMdd-HHmm</format>
            <sourceReferences>
              <generate>true</generate>
            </sourceReferences>
            <timestampProvider>jgit</timestampProvider>
            <jgit.ignore>pom.xml</jgit.ignore>
            <jgit.dirtyWorkingTree>warning</jgit.dirtyWorkingTree>
            <additionalFileSets>
              <fileSet>
                <directory>${project.build.outputDirectory}</directory>
                <includes>
                  <include>about.mappings</include>
                </includes>
              </fileSet>
            </additionalFileSets>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.eclipse.tycho.extras</groupId>
              <artifactId>tycho-sourceref-jgit</artifactId>
              <version>${tycho-version}</version>
            </dependency>
            <dependency>
              <groupId>org.eclipse.tycho.extras</groupId>
              <artifactId>tycho-buildtimestamp-jgit</artifactId>
              <version>${tycho-version}</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-surefire-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-source-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho.extras</groupId>
          <artifactId>tycho-source-feature-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho.extras</groupId>
          <artifactId>tycho-pack200a-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.cbi.maven.plugins</groupId>
          <artifactId>eclipse-jarsigner-plugin</artifactId>
          <version>${jarsigner-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho.extras</groupId>
          <artifactId>tycho-pack200b-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
        <plugin>
          <groupId>org.eclipse.tycho</groupId>
          <artifactId>tycho-p2-plugin</artifactId>
          <version>${tycho-version}</version>
        </plugin>
      </plugins>
    </pluginManagement>

  </build>

  <profiles>
    <profile>
      <id>macosx-jvm-flags</id>
      <activation>
        <os><family>mac</family></os>
      </activation>
      <properties>
        <os-jvm-flags>-XstartOnFirstThread</os-jvm-flags>
      </properties>
    </profile>

    <profile>
      <id>modules</id>
      <activation>
        <property>
          <name>MAVEN_BUILD</name>
          <value>!false</value>
        </property>
      </activation>
      <modules>
        <module>org.eclipse.userstorage</module>
        <module>org.eclipse.userstorage-feature</module>
        <module>org.eclipse.userstorage.oauth</module>
        <module>org.eclipse.userstorage.oauth.tests</module>
        <module>org.eclipse.userstorage.releng</module>
        <module>org.eclipse.userstorage.sdk</module>
        <module>org.eclipse.userstorage.sdk-feature</module>
        <module>org.eclipse.userstorage.site</module>
        <module>org.eclipse.userstorage.tests</module>
        <module>org.eclipse.userstorage.ui</module>
      </modules>
    </profile>

    <profile>
      <id>pack-and-sign</id>
      <activation>
        <property>
          <name>PACK_AND_SIGN</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.eclipse.tycho.extras</groupId>
            <artifactId>tycho-pack200a-plugin</artifactId>
            <executions>
              <execution>
                <id>pack200-normalize</id>
                <goals>
                  <goal>normalize</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.eclipse.cbi.maven.plugins</groupId>
            <artifactId>eclipse-jarsigner-plugin</artifactId>
            <executions>
              <execution>
                <id>sign</id>
                <goals>
                  <goal>sign</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.eclipse.tycho.extras</groupId>
            <artifactId>tycho-pack200b-plugin</artifactId>
            <executions>
              <execution>
                <id>pack200-pack</id>
                <goals>
                  <goal>pack</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>tycho-p2-plugin</artifactId>
            <executions>
              <execution>
                <id>attach-p2-metadata</id>
                <phase>package</phase>
                <goals>
                  <goal>p2-metadata</goal>
                </goals>
              </execution>
            </executions>
            <configuration>
              <defaultP2Metadata>false</defaultP2Metadata>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <id>eclipse-target-4.5</id>
      <activation>
        <property>
          <name>!eclipse.target</name>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>target-platform-configuration</artifactId>
            <version>${tycho-version}</version>
            <configuration>
              <target>
                <artifact>
                  <groupId>org.eclipse.userstorage</groupId>
                  <artifactId>org.eclipse.userstorage.releng</artifactId>
                  <version>1.0.0-SNAPSHOT</version>
                </artifact>
              </target>
              <!--executionEnvironmentDefault>JavaSE-${javaVersion}</executionEnvironmentDefault-->
              <includePackedArtifacts>true</includePackedArtifacts>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <!-- this target should only be used for testing purposes -->
      <id>target-eclipse-release</id>
      <activation>
        <property>
          <name>eclipse.target</name>
        </property>
      </activation>
      <build>
        <pluginManagement>
          <plugins>
            <plugin>
              <groupId>org.eclipse.tycho</groupId>
              <artifactId>tycho-compiler-plugin</artifactId>
              <version>${tycho-version}</version>
              <configuration>
		<!--
                  tycho-compiler-plugin's useProjectSettings
		  defaults to true, such that it will use the JDT
                  compiler source and target settings.  These are
                  currently set to JavaSE 1.6.  This setting causes
                  issues for Eclipse Neon (Platform 4.6) or later
                  as they target JavaSE 1.8 and so their .class files
                  result in version errors.  Fix this problem by
                  forcing a later target.
                -->
                <target>11</target>
              </configuration>
            </plugin>
          </plugins>
        </pluginManagement>
      </build>
      <repositories>
        <repository>
          <id>eclipse-named</id>
          <layout>p2</layout>
          <url>http://download.eclipse.org/releases/${eclipse.target}</url>
        </repository>
        <repository>
          <id>orbit-latest</id>
          <layout>p2</layout>
          <url>http://download.eclipse.org/tools/orbit/downloads/latest-R</url>
        </repository>
      </repositories>
    </profile>

    <profile>
      <!-- this target should only be used for testing purposes -->
      <id>eclipse-target-latest</id>
      <activation>
        <property>
          <name>eclipse.target</name>
          <value>latest</value>
        </property>
      </activation>
      <repositories>
        <repository>
          <id>eclipse-jetty-10</id>
          <layout>p2</layout>
          <url>https://download.eclipse.org/eclipse/jetty/10.0.2/</url>
        </repository>
      </repositories>
    </profile>

  </profiles>

</project>
