#!/bin/bash
# Release a milestone (really, an RC) as a formal release

if [[ "$GIT" == "" ]]; then
  GIT=$WORKSPACE/git
fi

if [[ "$SCRIPTS" == "" ]]; then
  SCRIPTS=$GIT/org.eclipse.userstorage.releng/hudson
fi

if [[ "$DOWNLOADS" == "" ]]; then
  DOWNLOADS=/home/data/httpd/download.eclipse.org/usssdk
fi

set -o nounset
set -o errexit

# the directory under $DOWNLOADS/drops to the milestone drop
# e.g., milestone/S20170529-101518-1.1-RC2
MILESTONESRC=$1

# the release name
# e.g., 1.1
RELEASELABEL="$2"

FOLDER=$RELEASELABEL

BUILD_TYPE=release
BUILD_KEY=$RELEASELABEL
BUILD_LABEL=""

BUILDS=$DOWNLOADS/builds
UPDATES=$DOWNLOADS/updates
DROPS=$DOWNLOADS/drops
DROP_TYPE=$DROPS/$BUILD_TYPE
DROP=$DROP_TYPE/$FOLDER
DROPSRC=$DROPS/$MILESTONESRC

if [ ! -d "$DROPSRC" ]; then
    echo "milestone does not exist: $DROPSRC" 1>&2
    exit 1
fi

mkdir -p $DOWNLOADS
mkdir -p $BUILDS
mkdir -p $UPDATES
mkdir -p $DROPS
mkdir -p $DROP_TYPE


###################
# DOWNLOADS/DROPS #
###################

echo "Promoting $DROPSRC"
rm -rf $DROP
cp -a $DROPSRC $DROP
$BASH $SCRIPTS/adjustArtifactRepository.sh \
  $DROP \
  $DROP \
  "User Storage Updates $FOLDER" \
  $BUILD_TYPE

#####################
# DOWNLOADS/UPDATES #
#####################

cd $WORKSPACE
rm -rf $UPDATES.tmp
cp -a $UPDATES $UPDATES.tmp

$BASH $SCRIPTS/composeRepositories.sh \
  "$DOWNLOADS" \
  "$BUILD_TYPE" \
  "$BUILD_KEY" \
  "$BUILD_LABEL"

mkdir -p $UPDATES.tmp/$BUILD_TYPE/latest
cp -a $DROP/org.eclipse.userstorage.site.zip $UPDATES.tmp/$BUILD_TYPE/latest

mkdir -p $UPDATES.tmp/latest
cp -a $DROP/org.eclipse.userstorage.site.zip $UPDATES.tmp/latest

mv $UPDATES $UPDATES.bak; mv $UPDATES.tmp $UPDATES

echo ""
