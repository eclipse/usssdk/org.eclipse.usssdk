/*
 * Copyright (c) 2016 Manumitting Technologies Inc and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Manumitting Technologies Inc - initial API and implementation
 */
package org.eclipse.userstorage.sdk.browser;

public class Constants {
  
  private static final String OAUTH_CLIENT_ID = "@uss.oauth.client.id@";
  private static final String OAUTH_CLIENT_SECRET = "@uss.oauth.client.secret@";

  public static String getOAuthClientId() {
    String clientId = System.getProperty("uss.oauth.client.id");
    if(clientId != null)
    {
      return clientId;
    }
    return OAUTH_CLIENT_ID;
  }

  public static String getOAuthClientSecret() {
    String clientSecret = System.getProperty("uss.oauth.client.secret");
    if (clientSecret != null)
    {
      return clientSecret;
    }
    return OAUTH_CLIENT_SECRET;
  }
}
