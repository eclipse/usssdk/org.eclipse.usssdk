package org.eclipse.userstorage.oauth;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class OAuthParametersTest {
	static class TestOAuthParameters extends OAuthParameters {
		/**
		 * Client ID: abcdefg
		 * Client Secret: 0123456789
		 */
		private static final String CLIENT_ID = "af48ccf2cfd9eb82";
		private static final String CLIENT_SECRET = "9a8ebe36ffb9f81d97af86";
		private static final String CLIENT_KEY = "cb8cce2aaf96aabf8c05";

		@Override
		protected String getServiceName() {
			return "test";
		}

		@Override
		protected String getDefaultClientId() {
			return CLIENT_ID;
		}

		@Override
		protected String getDefaultClientSecret() {
			return CLIENT_SECRET;
		}

		@Override
		protected String getDefaultClientKey() {
			return CLIENT_KEY;
		}
	}
	
	@Test
	public void testDecryption() {
		OAuthParameters parms = new TestOAuthParameters();
		assertThat(parms.getDecryptedClientID(), is("abcdefg"));
		assertThat(parms.getDecryptedClientSecret(), is("0123456789"));
	}
}
