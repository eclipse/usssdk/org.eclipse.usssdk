package org.eclipse.userstorage.oauth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import org.eclipse.userstorage.internal.oauth.AuthToken;
import org.junit.Test;

public class AuthTokenTest {

	@Test
	public void testParse_rfc6749() {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"uss_retrieve\"," + "\"expires_in\":3600}";
		AuthToken token = new AuthToken(serializedForm, new Date(0));
		assertEquals("a8f8a49232520e7d621b1ae1235a35665e27344d", token.getAccessToken());
		assertEquals("Bearer", token.getTokenType());
		assertEquals(new HashSet<String>(Collections.singleton("uss_retrieve")), token.getScopes());
		assertNull(token.getRefreshToken());
		assertTrue(token.isExpired());
	}

	@Test
	public void testParse_oauthDrupal() {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"uss_retrieve\"," + "\"expires_in\":\"3600\"}";
		AuthToken token = new AuthToken(serializedForm, new Date(0));
		assertEquals("a8f8a49232520e7d621b1ae1235a35665e27344d", token.getAccessToken());
		assertEquals("Bearer", token.getTokenType());
		assertEquals(new HashSet<String>(Collections.singleton("uss_retrieve")), token.getScopes());
		assertNull(token.getRefreshToken());
		assertTrue(token.isExpired());
	}

	@Test
	public void testDeserialization() {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"uss_retrieve\","
				+ "\"_expires_\":\"Mon, 07 Nov 2016 22:19:17 GMT\"}";
		AuthToken token = AuthToken.deserialize(serializedForm);
		assertEquals("a8f8a49232520e7d621b1ae1235a35665e27344d", token.getAccessToken());
		assertEquals("Bearer", token.getTokenType());
		assertEquals(new HashSet<String>(Collections.singleton("uss_retrieve")), token.getScopes());
		assertNull(token.getRefreshToken());
		assertTrue(token.isExpired());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyString() {
		AuthToken.deserialize("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBrokenDeserialization() {
		AuthToken.deserialize("{broken}");
	}

	@Test
	public void testReserialization() throws IllegalArgumentException, IOException {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"uss_retrieve\","
				+ "\"_expires_\":\"Mon, 07 Nov 2016 22:19:17 GMT\"}";
		AuthToken original = AuthToken.deserialize(serializedForm);
		AuthToken reserialized = AuthToken.deserialize(original.serialize());
		assertEquals(reserialized.getAccessToken(), original.getAccessToken());
		assertEquals(reserialized.getTokenType(), original.getTokenType());
		assertEquals(reserialized.getScopes(), original.getScopes());
		assertEquals(reserialized.getRefreshToken(), original.getRefreshToken());
		assertEquals(reserialized.isExpired(), original.isExpired());
	}

	@Test
	public void testNeverExpires() {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"uss_retrieve\"}";
		AuthToken token = AuthToken.deserialize(serializedForm);
		assertFalse(token.isExpired());
	}

}
