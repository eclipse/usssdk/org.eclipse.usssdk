package org.eclipse.userstorage.oauth;

import java.util.regex.Pattern;

import org.hamcrest.CustomTypeSafeMatcher;

public class RegexMatcher extends CustomTypeSafeMatcher<String> {
	public static RegexMatcher matches(String regex) {
		return matches(Pattern.compile(regex));
	}

	public static RegexMatcher matches(String regex, int flags) {
		return matches(Pattern.compile(regex, flags));
	}

	public static RegexMatcher matches(Pattern pattern) {
		return new RegexMatcher(pattern);
	}

	private Pattern pattern;

	private RegexMatcher(Pattern pattern) {
		super("matches regex '" + pattern.pattern().replace("\n", "\\n") + "'");
		this.pattern = pattern;
	}

	@Override
	protected boolean matchesSafely(String item) {
		return pattern.matcher(item).matches();
	}
}
