package org.eclipse.userstorage.oauth;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import org.eclipse.userstorage.IStorageService;
import org.eclipse.userstorage.internal.Session;
import org.eclipse.userstorage.internal.oauth.UIFacade;
import org.eclipse.userstorage.spi.Credentials;

import org.apache.http.client.fluent.Request;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.net.URI;
import java.net.URISyntaxException;

public class EclipseOAuthCredentialsProviderTest {

	@Test
	public void testClientDetails() throws URISyntaxException {
		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider("clientId", "clientSecret",
				new String[] { "scope1", "scope2" }, new URI("http://localhost"));
		assertEquals("clientId", provider.getClientId());
		assertEquals("clientSecret", provider.getClientSecret());
		assertArrayEquals(new String[] { "scope1", "scope2" }, provider.getScopes());
		assertEquals(new URI("http://localhost"), provider.getRegisteredCallback());
	}

	@Test
	public void testIsValid_InvalidCredentials() throws URISyntaxException {
		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider("clientId", "clientSecret",
				new String[] { "scope1", "scope2" }, new URI("http://localhost"));

		assertFalse(provider.isValid(new Credentials("", "blah")));
		assertFalse(provider.isValid(new Credentials("username", "")));
		assertFalse(provider.isValid(new Credentials("username", "blah")));
	}

	@Test
	public void testIsValid_ExpiredCredentials() throws URISyntaxException {
		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider("clientId", "clientSecret",
				new String[] { "scope1", "scope2" }, new URI("http://localhost"));
		// expired token
		assertFalse(provider.isValid(new Credentials("username",
				"{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\"," + "\"token_type\":\"Bearer\","
						+ "\"scope\":\"scope1\"," + "\"_expires_\":\"Mon, 07 Nov 2016 22:19:17 GMT\"}")));
	}

	@Test
	public void testIsValid_InsufficientScope() throws URISyntaxException {
		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider("clientId", "clientSecret",
				new String[] { "scope1", "scope2" }, new URI("http://localhost"));
		// expired token
		assertFalse(provider
				.isValid(new Credentials("username", "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
						+ "\"token_type\":\"Bearer\"," + "\"scope\":\"scope1\"}")));
	}

	@Test
	public void testConfigureRequest() throws URISyntaxException {
		String serializedForm = "{\"access_token\":\"a8f8a49232520e7d621b1ae1235a35665e27344d\","
				+ "\"token_type\":\"Bearer\"," + "\"scope\":\"scope1 scope2\"}";
		Credentials credentials = new Credentials("username", serializedForm);

		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider("clientId", "clientSecret",
				new String[] { "scope1" }, new URI("http://localhost"));
		assertTrue(provider.isValid(credentials));

		Request request = provider.configureRequest(Request.Get("http://localhost"), new URI("http://nonsensical"),
				credentials);
		assertNotNull(request);
		assertThat(Session.formatRequest(request),
				CoreMatchers.containsString("Authorization: Bearer a8f8a49232520e7d621b1ae1235a35665e27344d"));
	}

	@Test
	public void testProvideCredentials_nonInteractive() {
		IStorageService service = Mockito.mock(IStorageService.class);

		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider(
				URI.create("http://does.not.exist"), "clientId", "clientSecret", new String[] { "scope1" },
				URI.create("http://localhost"));
		provider.uiFacade = Mockito.mock(UIFacade.class);

		provider.setInteractive(false);
		Credentials credential = provider.provideCredentials(service, false);
		Mockito.verifyZeroInteractions(provider.uiFacade);
		assertNull(credential); // should fast-exit
	}

	@Test
	public void testProvideCredentials_interactive() {
		IStorageService service = Mockito.mock(IStorageService.class);
		Mockito.when(service.getServiceLabel()).thenReturn("provider");

		URI authServiceURI = URI.create("http://does.not.exist");
		URI callbackURI = URI.create("http://localhost");
		EclipseOAuthCredentialsProvider provider = new EclipseOAuthCredentialsProvider(authServiceURI, "clientId",
				"clientSecret", new String[] { "scope1" }, callbackURI);
		provider.uiFacade = Mockito.mock(UIFacade.class);
		Mockito.when(provider.uiFacade.obtainAuthCode(anyString(), any(URI.class), any(URI.class))).thenReturn(null);

		assertTrue(provider.isInteractive());
		Credentials credential = provider.provideCredentials(service, false);

		assertNull(credential); // facade.obtainAuthCode => null returns null
		ArgumentCaptor<URI> authCaptor = ArgumentCaptor.forClass(URI.class);
		Mockito.verify(provider.uiFacade).obtainAuthCode(eq("provider"), authCaptor.capture(), eq(callbackURI));
		assertThat(authCaptor.getValue().toString(), CoreMatchers.startsWith(authServiceURI.toString()));
	}

}
