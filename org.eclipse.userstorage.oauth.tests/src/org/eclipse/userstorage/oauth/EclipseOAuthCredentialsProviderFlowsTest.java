package org.eclipse.userstorage.oauth;

import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.userstorage.IStorageService.Dynamic;
import org.eclipse.userstorage.StorageFactory;
import org.eclipse.userstorage.internal.Storage;
import org.eclipse.userstorage.internal.StorageService;
import org.eclipse.userstorage.internal.StorageServiceRegistry;
import org.eclipse.userstorage.spi.Credentials;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EclipseOAuthCredentialsProviderFlowsTest {
	private EclipseOAuthCredentialsProvider provider;
	private MockServer authServer;
	private MockServer ussServer;

	@Before
	public void setUp() throws URISyntaxException, IOException {
		authServer = new MockServer(-1).start();
		ussServer = new MockServer()
				.expect(allOf(MockServer.isGet("/uss/blob/"), MockServer.hasHeader("User-Agent", "uss/[0-9.]+"),
						MockServer.hasHeader("Content-Type", "application/json"),
						MockServer.hasHeader("Authorization", "Bearer accToken")),
						new String[] { "HTTP/1.1 200 OK\nContent-Type: application/json", "[]" })
				.start();
		Thread.yield();

		provider = new EclipseOAuthCredentialsProvider(authServer.getURI(), "clientId", "clientSecret",
				new String[] { "scope1" }, new URI("http://localhost/"));
		provider.setStateCode("abcdefg");
		provider.uiFacade = new HeadlessFacade();
	}

	@After
	public void tearDown() {
		if (authServer != null) {
			authServer.stop();
		}
		if (ussServer != null) {
			ussServer.stop();
		}
	}

	@Test
	public void successfulAuthorization() throws IOException {
		authServer.expect(
				allOf(MockServer.isGet("/oauth2/authorize"), MockServer.hasQueryParameter("response_type", "code"),
						MockServer.hasQueryParameter("client_id", "clientId"),
						MockServer.hasQueryParameter("client_secret", "clientSecret"),
						MockServer.hasQueryParameter("scope", "scope1"),
						MockServer.hasQueryParameter("redirect_uri", "http%3A%2F%2Flocalhost%2F"),
						MockServer.hasQueryParameter("state", "abcdefg")),
				"HTTP/1.1 302 OK\n" + "Location: http://localhost/?code=987654321&state=abcdefg\n\n");
		authServer.expect(
				allOf(MockServer.isPost("/oauth2/token"),
						MockServer.hasHeader("Content-Type", "application/x-www-form-urlencoded"),
						MockServer.hasBodyParameter("grant_type", "authorization_code"),
						MockServer.hasBodyParameter("client_id", "clientId"),
						MockServer.hasBodyParameter("client_secret", "clientSecret"),
						MockServer.hasBodyParameter("redirect_uri", "http%3A%2F%2Flocalhost%2F"),
						MockServer.hasBodyParameter("code", "987654321")),
				new String[] { "HTTP/1.1 200 OK\n" + "Content-Type: application/json\n",
						"{\"access_token\":\"accToken\",\"expires_in\":\"3600\",\"token_type\":\"Bearer\",\"scope\":\"scope1\",\"refresh_token\":\"refToken\"}" });

		Dynamic service = StorageServiceRegistry.INSTANCE.addService("foo", ussServer.getURI(), ussServer.getURI(),
				ussServer.getURI(), ussServer.getURI(), "http://localhost/tos");
		assertFalse(provider.hasCredentials(service));
		Credentials credential = provider.getCredentials(service);
		assertNull(credential);
		credential = provider.provideCredentials(service, false);
		assertNotNull(credential);

		assertTrue(authServer.isOK());
		assertEquals(2, authServer.getHandledRequests());

		Storage storage = new Storage(StorageFactory.DEFAULT, "abcdef", null);
		storage.setService(service);
		storage.setCredentialsProvider(provider);
		storage.getBlobs(1, 1);

		assertTrue(ussServer.isOK());
		assertTrue("USS server should have had a request", ussServer.getHandledRequests() >= 1);
	}

	@Test
	public void authorizationDenied() {
		authServer.expect(
				allOf(MockServer.isGet("/oauth2/authorize"), MockServer.hasQueryParameter("response_type", "code"),
						MockServer.hasQueryParameter("client_id", "clientId"),
						MockServer.hasQueryParameter("client_secret", "clientSecret"),
						MockServer.hasQueryParameter("scope", "scope1"),
						MockServer.hasQueryParameter("redirect_uri", "http%3A%2F%2Flocalhost%2F"),
						MockServer.hasQueryParameter("state", "abcdefg")),
				"HTTP/1.1 302 OK\n"
						+ "Location: http://localhost/?error=access_denied&error_description=denied&state=abcdefg\n\n");

		StorageService service = new StorageService("foo", ussServer.getURI(), ussServer.getURI(), ussServer.getURI(),
				ussServer.getURI(), "http://localhost/tos");
		assertFalse(provider.hasCredentials(service));
		Credentials credential = provider.getCredentials(service);
		assertNull(credential);
		credential = provider.provideCredentials(service, false);
		assertNull(credential);

		assertTrue(authServer.isOK());
		assertEquals(1, authServer.getHandledRequests());
	}

}
