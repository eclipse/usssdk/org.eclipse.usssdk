/*
 * Copyright (c) 2015 Eike Stepper (Berlin, Germany) and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package org.eclipse.userstorage.tests.util;

import org.eclipse.userstorage.IStorageService;
import org.eclipse.userstorage.internal.CredentialedSession;
import org.eclipse.userstorage.internal.Session;
import org.eclipse.userstorage.spi.AbstractCredentialsProvider;
import org.eclipse.userstorage.spi.Credentials;
import org.eclipse.userstorage.util.NoServiceException;

/**
 * @author Eike Stepper
 */
public class FixedCredentialsProvider extends AbstractCredentialsProvider
{
  public static final Credentials DEFAULT_CREDENTIALS = new Credentials("eclipse_test_123456789", "plaintext123456789");

  private static Credentials credentials = DEFAULT_CREDENTIALS;

  public FixedCredentialsProvider()
  {
  }

  @Override
  public Credentials provideCredentials(IStorageService service, boolean reauthentication)
  {
    return credentials;
  }

  public static Credentials setCredentials(Credentials credentials)
  {
    Credentials oldCredentials = FixedCredentialsProvider.credentials;
    FixedCredentialsProvider.credentials = credentials;
    return oldCredentials;
  }

  @Override
  public boolean hasCredentials(IStorageService service)
  {
    return credentials != null;
  }

  @Override
  public Credentials getCredentials(IStorageService service)
  {
    return credentials;
  }

  @Override
  public Session openSession(IStorageService service) throws NoServiceException
  {
    return new CredentialedSession(service, this);
  }

}
