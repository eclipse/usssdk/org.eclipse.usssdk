/*
 * Copyright (c) 2016 Manumitting Technologies Inc and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Manumitting Technologies Inc - initial API and implementation
 */
package org.eclipse.userstorage.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.eclipse.userstorage.internal.util.AES;

import org.junit.Test;

import java.security.GeneralSecurityException;

/**
 * Tests for the {@link AES} class.
 */
public class AESTests
{
  @Test
  public void testAES() throws GeneralSecurityException
  {
    char[] password = "this is a test".toCharArray();
    String text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i = 1; i < text.length(); i++)
    {
      String source = text.substring(0, i);
      String encryptedForm = AES.encrypt(source, password);
      assertNotEquals("No apparent encryption+encoding", source, encryptedForm);
      String decryptedForm = AES.decrypt(encryptedForm, password);
      assertEquals("Decrypted form is different", source, decryptedForm);
    }

  }
}
